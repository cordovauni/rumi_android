package pe.luminar.rumiapp.viewcontrollers.activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.TextView
import pe.luminar.rumiapp.R
import pe.luminar.rumiapp.viewcontrollers.fragments.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
            item -> return@OnNavigationItemSelectedListener navigateTo(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nav_view.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        nav_view.selectedItemId = R.id.navigation_home
    }

    private fun fragmentFor(item: MenuItem): Fragment {
        when(item.itemId){
            R.id.navigation_home -> {
                return HomeFragment()
            }
            R.id.navigation_group -> {
                return GroupFragment()
            }
            R.id.navigation_chat -> {
                return ChatFragment()
            }
            R.id.navigation_profile -> {
                return AccountFragment()
            }
        }
        return HomeFragment()
    }

    private fun navigateTo(item: MenuItem): Boolean {
        item.setChecked(true)
        return supportFragmentManager
            .beginTransaction()
            .replace(R.id.attendeeContent, fragmentFor(item))
            .commit() > 0
    }
}
